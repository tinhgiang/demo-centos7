package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoCentos7Application {

    public static void main(String[] args) {
        SpringApplication.run(DemoCentos7Application.class, args);
    }

}
