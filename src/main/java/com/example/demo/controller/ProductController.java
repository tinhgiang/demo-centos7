package com.example.demo.controller;

import com.example.demo.entity.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/products")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String list(Model model){
        model.addAttribute("products", productRepository.getListByStatus(1));
        return "products/list";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(Model model) {
        model.addAttribute("product", new Product());
        return "products/form";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String store(@Valid Product product, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "products/form";
        }
        product.setStatus(1);
        productRepository.save(product);
        return "redirect:/products";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable("id") int id, Model model) {
        model.addAttribute("product", productRepository.findById(id));
        return "products/edit";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public String update(@PathVariable("id") int id, @Valid Product product, BindingResult bindingResult, Model model) {
//         if(bindingResult.hasErrors()){
//             product.setId(id);
//             return "products/edit";
//         }
         productRepository.save(product);
         model.addAttribute("products", productRepository.findAll());
         return "redirect:/products";

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable int id, Model model) {
        Product product = productRepository.findById(id).get();
        if (product != null) {
          product.setStatus(-1);
          productRepository.save(product);
          return "redirect:/products";
        }
        return null;
    }
}
